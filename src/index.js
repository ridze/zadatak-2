'use strict';

let units = [];

class Unit {
	constructor(name, health = 100) {
		this.name = name;
		this.health = health;
	}

	rechargeTime() {
		return 1000 * this.health / 100;
	}

	damage() {
		return this.health / 100;
	}

	criticalChance() {
		return 10 - this.health / 10;
	}

	hasCritical() {
		return (Math.random() < this.criticalChance() / 100);
	}

	findTarget() {
		const otherAliveUnits = units.filter(unit => (unit !== this && unit.health > 0));
		if (otherAliveUnits.length) {
			const i = Math.floor(Math.random() * otherAliveUnits.length);
			return otherAliveUnits[i];
		}
		console.log(`${this.name} won the shooting.`);
		return false;
	}

	attack() {
		setTimeout(() => {
			const target = this.findTarget();
			if (this.health > 0 && target) {
				const currentDamage = this.damage() * this.hasCritical() ? 2 : 1;
				console.log(`${this.name} (HP:${this.health}): \nAttacking --> ${target.name}, damage: ${currentDamage}`);
				target.health -= currentDamage;
				console.log(`${target.name} HP: ${target.health}\n`);
				this.attack();
			}
		}, this.rechargeTime());
		return false;
	}
}

const tank = new Unit('Tank');
const infantry = new Unit('Infantry');
const helicopter = new Unit('Helicopter');
const cannon = new Unit('Cannon');
const airship = new Unit('Airship');

units = [tank, infantry, helicopter, cannon, airship];

const fight = (opponents) => {
	for (let i = 0; i < opponents.length; i += 1) {
		opponents[i].attack();
	}
};

fight(units);
